#!/bin/sh

aclocal || exit 
libtoolize --force || exit
autoconf || exit
automake -a -c || exit

./configure $@
